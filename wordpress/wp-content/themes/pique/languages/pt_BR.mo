��    K      t  e   �      `  
   a     l  	   o     y     ~     �     �  0   �  �   �  %   T  )   z  *   �     �     �     �          #  8   A     z          �     �     �  	   �  _   �  W   8	     �	     �	     �	     �	     �	     �	      
     
     
     "
  %   1
     W
     \
     c
     {
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     	  F        ]     t     �     �     �     �  \   �     #     /     <     J     _  �   x  )     T   ,  !   �     �     �    �     �     �     �               
       :     �   M  +   �  2   #  8   V  !   �     �     �  "   �     �  7        I     P     a  $        �     �  j   �  g   8     �     �     �     �     �     �     �                1  +   K     w  	        �     �     �     �     �     �     �     �     �     �          
          %     4     C  G   P     �  #   �     �     �     �       `        w     �     �     �  $   �  �   �  &   j  L   �  !   �                =               ;   3           "       A       !   4                 E   $       %   
   I                7      6       8      J   &       5          )           @      +                    :   '   9           ?   K                       C   (       G       H   <   >   F                 B                    D   *      ,   -   .   /   0   1   #                 2   	    % Comments ,  1 Comment 100% 25% 50% 75% A one-page scrolling theme for small businesses. Add a background image to your panel by setting a featured image in the page editor. If you don&rsquo;t select a page, this panel will not be displayed. Add an anchor menu to the front page. Add widgets here to appear in your footer Add widgets here to appear in your sidebar Alternate Panel Background Background Color Comment navigation Comments are closed. Configure your theme settings Continue reading %s <span class="meta-nav">&rarr;</span> Edit Edit %1$s %2$s Featured Image Opacity First Footer Widget Area Full-width Page Grid Page It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Karla font: on or offon Leave a comment Lora font: on or offon Main Accent Menu Settings Most Used Categories Newer Comments Next Nothing Found Older Comments Oops! That page can&rsquo;t be found. Page Pages: Panel & Menu Background Panel 1 Panel 2 Panel 3 Panel 4 Panel 5 Panel 6 Panel 7 Panel 8 Panel Content Post Posted in %1$s Previous Primary Menu Proudly powered by %s Read more %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Second Footer Widget Area Secondary Accent Secondary Menu Sidebar Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Testimonials Theme Options Theme: %1$s by %2$s. Third Footer Widget Area This replaces your custom menu&mdash;on the front page only&mdash;with an automatically-generated menu that links to each of your panels. Try looking in the monthly archives. %1$s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; http://wordpress.com/themes/pique http://wordpress.org/ post authorby %s PO-Revision-Date: 2017-03-23 08:23:09+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: GlotPress/2.4.0-alpha
Language: pt_BR
Project-Id-Version: WordPress.com - Themes - Pique
 % Comentários ,  1 Comentário 100% 25% 50% 75% Um tema de página-única rolante para pequenos negócios. Adicionar uma imagem de fundo ao seu painel configurando uma imagem destacada na página do editor. Se você não selecionar uma página, este painel não será exibido. Adicione um menu âncora na página inicial Adicione widgets aqui para aparecer em seu rodapé Adicione widgets aqui para aparecer em sua barra lateral Alternar plano de fundo do painel Cor de fundo Navegação dos comentários Os comentários estão encerrados. Configure seu tema Continuar lendo %s <span class="meta-nav">&rarr;</span> Editar Editar %1$s %2$s Opacidade da Imagem Destacada Primeira área de widgets do rodapé Página de largura cheia Pagina de grade Parece que nada foi encontrado neste lugar. Quem sabe você possa tentar um dos links abaixo ou uma busca? Parece que não encontramos o que você está procurando. Talvez a ferramenta de pesquisa possa ajudar. on Deixe um comentário ligada Cor principal Configurações do menu Categorias mais Usadas Comentários mais recentes Avançar Nenhum resultado Comentários mais antigos Ops! Essa página não pode ser encontrada. Página Páginas: Plano de fundo do menu e painel Painel 1 Painel 2 Painel 3 Painel 4 Painel 5 Painel 6 Painel 7 Painel 8 Painel de conteúdo Post Publicado em %1$s Anterior Menu Principal Mantido com %s Leia Mais %s Pronto para publicar seu primeiro post? <a href="%1$s">Comece aqui</a>. Resultados da busca por: %s Segunda área de widgets do rodapé Realce Secundário Menu Secundário Lateral Ir para conteúdo Não encontramos nada para estes termos de busca. Tente novamente com palavras-chave diferentes. Marcado %1$s Depoimentos Opções do tema Tema: %1$s por %2$s  Terceira área de widgets do rodapé Isso substitui seu menu customizado na página de frente somente, com um menu gerado automaticamente que faz links a cada um de seus painéis. Tente olhar nos arquivos mensais. %1$s Um comentário em &ldquo;%2$s&rdquo; %1$s comentários em &ldquo;%2$s&rdquo; http://wordpress.com/themes/pique http://br.wordpress.org/ por %s 
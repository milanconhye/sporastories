<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N~8FB/8$<iy~*foxT.#6(y`uT7[`:CKS+AJz<YB@*gAW1#M}E2$KN{]4vjZYK.BL');
define('SECURE_AUTH_KEY',  '#|[U8$bzcG8QulDKc}VS072#kzlo$H>gTc{||%JY30zPZj|?G($(yLfm{01O6#``');
define('LOGGED_IN_KEY',    'J2gs00dR:9(Aj8+QcxNKi(^u&^$Cia=VSW*leN>DWk>2ys~WclV}DqXl3G]A5h9w');
define('NONCE_KEY',        '-&EWYF[WKw&Tey)DKXwm^?hd0+Drf~<vr/0eh|jvsikwOks:?jw,q#{HIOo4NGw.');
define('AUTH_SALT',        't8!L@PwOSZ. dyHJh]On[Vb2d=T+{]de(?(~8gtOK/}^{7jb8U^$@JzB;8-T#MC*');
define('SECURE_AUTH_SALT', 't9.e8g!Z;/@]yHs/cJ];Gyj`*anUzS*orxeZ~TXsT>a!RCF6vG<:1Hb].~]?un%@');
define('LOGGED_IN_SALT',   '{{Vbj;]Q@hD>fz]yp>4c=6ZzAfT*Kq~,Os9lZ>;zousN#3B<(`fk9c*U0X$mQ*+!');
define('NONCE_SALT',       '#R&&Gkx3eye+5X6|{=qSL>n]#GXA7ii@IM~[,kAb7WrB,A]sTlzbyo)!#*;=q*&Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
